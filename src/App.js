import React from 'react'
import './App.css';

import Header from './components/header'
import Home from './components/home'
import {Routes, Route} from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route exact path='/' element={<Home />}/>
      </Routes>
    </div>
  );
}

export default App;
