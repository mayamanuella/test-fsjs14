import React, {useState, useEffect} from 'react'
import img1 from '../assets/images/1.jpg'
import img2 from '../assets/images/2.jpg'
import img3 from '../assets/images/3.jpg'
import img4 from '../assets/images/4.jpg'
import img5 from '../assets/images/5.jpg'
import img6 from '../assets/images/6.jpg'


const Home = (props)=>{
    
    const slides = [
                	{ image: img1, legend: 'Street Art'          },
                	{ image: img2, legend: 'Fast Lane'           },
                	{ image: img3, legend: 'Colorful Building'   },
                	{ image: img4, legend: 'Skyscrapers'         },
                	{ image: img5, legend: 'City by night'       },
                	{ image: img6, legend: 'Tour Eiffel la nuit' }
                ];
                
    const [timer, setTimer] = useState(null)
    const [numero, setNumero] = useState(0)
    
    const getRandomInteger = (min, max) =>{
	    return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    
    const randomImg = ()=>{
        let index;
        do{
            index = getRandomInteger(0, slides.length -1)
        }while(index === numero)
        
        setNumero(index)
    }
    
    const playPause = ()=>{
        
        
        if(timer === null){
            setTimer(setInterval(()=>{
                randomImg()
            }, 3000))
        }else{
            setTimer(clearInterval(timer))
            setTimer(null)
        }
    }
    
    return (
        <main>
            <button
                onClick={(e)=>{
                    if(numero - 1 < 0){
                        setNumero(slides.length - 1)
                    }else{
                        setNumero(numero -1)
                    }
                }}
            >
                Précedent
            </button>
            
            <button
                onClick={playPause}
            >
                play/pause
            </button>
            
            <button
                onClick={(e)=>{
                    if((numero +1) === slides.length){
                        setNumero(0)
                    }else{
                        setNumero(numero + 1)
                    }
                }}
            >
                Suivant
            </button>
            
            <button
                onClick={randomImg}
            >
                Au pif
            </button>
            
            <figure>
                <img src={slides[numero].image}/>
                <figcaption>{slides[numero].legend}</figcaption>
            </figure>
            
        </main>
    )
    
}

export default Home